(function(scope) {
	
	var registry = {},
		styleEl = document.createElement('style');

	document.head.appendChild(styleEl);

	function generateCssString(className, props) {
		var str = `.${className} { `;
		for(var key in props) {
			var dasherizedKey = camelToDash(key);
			str += `${dasherizedKey}:${props[key]}; `;
		}
		str += `}`;
		return str;
	}

	function camelToDash(str) {
		return str.replace(/\W+/g, '-')
		.replace(/([a-z\d])([A-Z])/g, '$1-$2');
	}

	function dashToCamel(str) {
		return str.replace(/\W+(.)/g, function (x, chr) {
			return chr.toUpperCase();
		});
	}

	scope.style = {

		getState:function(name) {
			return statesRegistry[name];
		},

		define: function (style) {
			var defined = [];
			for(var prop in style) {
				var definition = style[prop],
					name = prop,
					styles = definition,
					cssString = generateCssString(name, styles);
				registry[name] = {
					node: document.createTextNode(cssString),
					styles: styles,
				};
				styleEl.appendChild(registry[name].node);
				defined.push(registry[name]);
			}
			return defined;
		}

	};

})(this);
