(function(scope) {
	
	var store = {};

	function parseSchema(table) {
		var testRow = table[0],
			types = {};
		for(var key in testRow) {
			types[key] = typeof testRow[key];	
		}
		return types;
	}

	function updateImportedRowIds(table) {
		var data = store[table.db][table.id],
			i = 0,
			l = data.length;
		for(; i < l; i++) {
			table.inc_id ++;
			data[i].id = table.inc_id;
		}
	}

	function DB(name) {
		this.id = name;
		store[name] = {};	
	}

	DB.prototype = {

		get db() { return store[this.id] },

		getTable: function(tableName) {
			return this[tableName];
		},

		createTable: function(tableName) {
			return this[tableName] = new Table(this.id, tableName);	
		},

		importTable: function(tableName, table) {
			var newTable = this.createTable(tableName);
			store[this.id][tableName] = JSON.parse(table);
			var definition = parseSchema(store[this.id][tableName]);
			newTable.defineSchema(definition);
			updateImportedRowIds(newTable);
			return newTable;
		},

		dump: function(fn) {
			return fn.apply(null, [
				JSON.stringify(store[this.id])
			]);
		}

	};

	function Table(dbName, tableName) {
		this.id = tableName;
		this.db = dbName;
		this.inc_id = 0;	
		this.schema = null;
		store[dbName][tableName] = store[dbName][tableName] || [];
	}

	Table.prototype = {

		get table() { return store[this.db][this.id] },

		defineSchema: function(definition) {
			if(!this.schema) this.schema = {};
			var schema = this.schema,
				tableName = this.id,
				dbName = this.db;
			for(var key in definition) {
				(function(key) {
					schema[key] = function(value) {
						if(typeof value === definition[key]) {
							return value;
						} else {
							throw new Error(JSON.stringify({
								ERROR:'invalid type',
								info: {
									value: value,
									key: key,
									table:tableName,
									db:dbName
								}
							}));
						}	
					}		
				})(key);
			}	
			return this.schema;
		},

		insertRow: function(row) {
			if(this.schema) {
				this.inc_id ++;
				var newRow = {};
				for(var key in this.schema) {
					if(row[key]) {
						newRow[key] = this.schema[key](row[key]);
					} else {
						newRow[key] = null;
					}
				}	
				newRow.id = this.inc_id;
				return store[this.db][this.id].push(newRow);
			}	
		},

		deleteRow: function(row) {
			var i = store[this.db][this.id].length,
				output;
			while(i--) {
				var item = store[this.db][this.id][i];
				if(row.id === item.id) {
					output = store[this.db][this.id].splice(i, 1);
				}
			}
			return output;
		},

		updateRow: function(row, update) {
			var i = store[this.db][this.id].length,
				output;
			while(i--) {
				var item = store[this.db][this.id][i];
				if(row.id === item.id) {
					for(var key in update) {
						item[key] = this.schema[key](update[key]);
					}
					output = item;
				}
			}
			return output;
		},

		query: function(criteria) {
			var output = [],
				tests = 0,
				matches = {};
			for(var key in criteria) {
				var i = store[this.db][this.id].length;
				while(i--) {
					var item = store[this.db][this.id][i];
					if(!matches[item.id]) matches[item.id] = [];
					if(criteria[key](item[key])) {
						matches[item.id].push(item);	
					}
				}
				tests ++;
			}
			for(var match in matches) {
				if(matches[match].length === tests) {
					output.push(matches[match][0]);
				}
			}
			return output;
		},

		dump: function(fn) {
			return fn.apply(null, [
				JSON.stringify(store[this.db][this.id])
			]);
		}

	};

	scope.model = {

		get store() { return store },

		create: function(name) {
			return new DB(name);
		},

		import: function(name, db) {
			var newDB = new DB(name),
				dbType = typeof db;
			if(dbType === 'string') {
				store[name] = JSON.parse(db);
			} else if(dbType === 'object') {
				store[name] = db;
			}
			for(var key in store[name]) {
				var definition = parseSchema(store[name][key]);
				var table = newDB.createTable(key);
				table.defineSchema(definition);
				updateImportedRowIds(table);
			}	
			return newDB;
		}

	};

})(this);
