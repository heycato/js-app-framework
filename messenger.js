(function(scope) {
	
	function Messenger() {
		this.messages = {};
		this.subscribers = [];
	}

	Messenger.prototype = {

		listen: function(message, callback) {
			if(!this.messages[message]) {
				this.messages[message] = [];
			}
			this.messages[message].push(callback);
			return this.messages[message];
		},

		ignore: function(message, callback) {
			var i = this.messages[message].indexOf(callback),
				output = null;
			if(i > -1) {
				output = this.message[message].splice(i, 1);	
			}
			return output;
		},

		notify: function(message, payload) {
			var i = this.messages[message].length,
				output = [];
			while(i--) {
				output.push(this.messages[message][i].apply(null, [payload]));
			}
			return output;
		}, 

		broadcast: function(message, payload) {
			var i = this.subscribers.length;
			while(i--) {
				var messenger = this.subscribers[i];
				messenger.notify(message, payload);
			}	
			return {
				sentTo:this.subscribers,
				message:message,
				payload:payload
			}
		},

		subscribe: function(messenger) {
			messenger.subscribers.push(this);
		}	

	};

	scope.messenger = {

		create: function() {
			return new Messenger();
		}

	};

})(this);
