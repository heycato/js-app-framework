(function(scope) {

	var pool = {},
		registry = {};

	function getElement(type) {
		var el = null;
		if(pool[type] && pool[type].length > 0) {
			el = pool[type].pop();	
		} else if(typeof type === 'object') {
			el = type;
		} else {
			el = document.createElement(type || 'div');
		}
		return el;
	}

	function addToPool(orphan) {
		var type = orphan.type;
		if(!pool[type]) pool[type] = [];
		pool[type].push(orphan);	
		return pool[type];
	}

	function buildTransform() {
		var pos = this.position;
		return `translate3d(${pos[0]}px,${pos[1]}px,0px)`;
	}

	function ViewElement(type) {
		var el = getElement(type);
		if(typeof el === 'ViewElement') return el;
		this.element = el;
		this.type = el.tagName;
		this.uniqueId = Math.random().toString(36).substr(2, 10);
		this.classes = [];
		this.children = [];
		this.messages = {};
		this.styles = {};
		this.position = [0,0];
		this.size = [0,0];
	}

	ViewElement.prototype = {

		listen: function(message, callback) {
			if(!this.messages[message]) this.messages[message] = [];	
			var addDomListener = this.element.addEventListener;
			this.messages[message].push(callback);
			addDomListener.apply(this.element, [message, callback]);
		},

		ignore: function(message, callback) {
			if(this.messages[message]) {
				var removeDomListener = this.element.removeEventListener,
					index = this.messages[message].indexOf(callback);
				if(index > -1) {
					removeDomListener.apply(this.element, [message, callback]);
					this.messages[message].splice(i, 1);
				}
			}
		},

		add: function() {
			var i = 0,
				l = arguments.length;
			for(; i < l; i ++) {
				var child = arguments[i];		
				child.parent = this;
				this.children.push(child);
				this.element.appendChild(child.element);
			}
			return this.children;
		},

		remove: function() {
			var i = arguments.length,
				orphans = [];
			while(i --) {
				var index = this.children.indexOf(arguments[i]);
				if(index > -1) {
					var orphan = this.children.splice(index, 1)[0];
					console.log(orphan);
					orphan.parent = null;
					this.element.removeChild(orphan.element);
					orphans.push(orphan);
				}
			}
			this.recycle.apply(this, orphans);
			return this.children;
		},

		x: function(val) {
			this.position[0] = val || this.position[0];
			this.element.style.transform = buildTransform.call(this);
			return this.position[0];
		},
		
		y: function(val) {
			this.position[1] = val || this.position[1];
			this.element.style.transform = buildTransform.call(this);
			return this.position[1];
		},
		
		width: function(val) {
			this.size[0] = val || this.size[0];
			this.element.style.width = this.size[0] + 'px';
			return this.size[0];
		},

		height: function(val) {
			this.size[1] = val || this.size[1];
			this.element.style.height = this.size[1] + 'px';
			return this.size[1];
		},

		addClass: function() {
			var i = 0,
				l = arguments.length;
			for(; i < l; i ++) {
				this.classes.push(arguments[i]);
			}
			this.element.className = this.classes.join(' ');
			return this.classes;
		},

		removeClass: function() {
			var i = arguments.length;
			while(i --) {
				var index = this.classes.indexOf(arguments[i]);
				if(index > -1) this.classes.splice(index, 1);
			}
			this.element.className = this.classes.join(' ');
			return this.classes;
		},

		setSrc: function(src) {
			this.src = src;
			this.element.src = src;
			return this.src;
		},

		recycle: function() {
			var i = arguments.length;
			while(i --) {
				var orphan = arguments[i];
				console.log(orphan);
				orphan.resetClasses()
					.resetChildren()
					.resetListeners()
					.resetAttributes();
				addToPool(orphan);
			}
		},

		resetClasses: function() {
			console.log(this);
			this.classes = [];
			this.element.className = '';
			return this;
		},	

		resetChildren: function() {
			this.remove.apply(this , this.children);
			return this;
		},	

		resetListeners: function() {
			for(var listener in this.listeners) {
				var callback = this.listeners[listener];
				this.ignore(listener, callback);
			}
			return this;
		},	

		resetAttributes: function() {
			this.element.style.transform = 'translate3d(0px,0px,0px)';	
			this.element.style.width = '0px';	
			this.element.style.height = '0px';	
			this.position = [0,0];
			this.size = [0,0];
			this.src = '';
			this.element.src = '';
			return this;
		}
			
	};

	scope.element = {
		
		get pool() { return pool },

		create: function(type) {
			return new ViewElement(type);
		}

	};

})(this);
