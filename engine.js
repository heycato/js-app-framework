(function(scope) {

	var rAF = requestAnimationFrame,
		cAF = cancelAnimationFrame,
		CYCLE_DURATION = 10,
		EXEC_QUEUE = [],
		EXEC_DEFER = [];

	scope.engine = {

		execLoop: function(elapsed, start, commands) {
			while(elapsed < CYCLE_DURATION && commands.length > 0) {
				commands.shift().call(); // execute next in que
				elapsed = Date.now() - start;
			}
			return elapsed;
		},
		
		nextCycle: function() {
			if(engine.onCycleStart) engine.onCycleStart.call(null, {
				queueCount: EXEC_QUEUE.length,
				deferCount: EXEC_DEFER.length
			});
			var start = Date.now(),
				elapsed = engine.execLoop(0, start, EXEC_QUEUE);
			elapsed = engine.execLoop(elapsed, start, EXEC_DEFER);
			if(engine.halt) {
				cAF(engine.raf);
				engine.halt = false;
			} else {
				rAF(engine.nextCycle);
			}
			if(engine.onCycleEnd) engine.onCycleEnd.call(null, {
				timeRemaining: CYCLE_DURATION - elapsed,
				queueCount: EXEC_QUEUE.length,
				deferCount: EXEC_DEFER.length
			});
		},

		stop: function() {
			if(engine.raf) engine.halt = true;
		},

		start: function() {
			engine.raf = rAF(engine.nextCycle);
		},

		queue: function() {
		var i = 0,
				l = arguments.length;
			for(; i < l; i ++) {
				EXEC_QUEUE.push(arguments[i]);
			}
		},

		defer: function() {
			var i = 0,
				l = arguments.length;
			for(; i < l; i ++) {
				EXEC_DEFER.push(arguments[i]);
			}
		}

	};

	scope.engine.start();

})(this);
